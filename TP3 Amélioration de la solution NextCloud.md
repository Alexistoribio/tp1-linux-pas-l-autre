# TP3 Amélioration de la solution NextCloud

# Module 2 : Réplication de base de données

Il y a plein de façons de mettre en place de la réplication de base données de type MySQL (comme MariaDB).

Une réplication simple est une configuration de type "master-slave". Un des deux serveurs est le *master* l'autre est un *slave*.

Le *master* est celui qui reçoit les requêtes SQL (des applications comme NextCloud) et qui les traite.
Le *slave* ne fait que répliquer les donneés que le *master* possède.

Pour ce module, vous aurez besoin d'un deuxième serveur de base de données.

✨ **Bonus** : Faire en sorte que l'utilisateur créé en base de données ne soit utilisable que depuis l'autre serveur de base de données

- inspirez-vous de la création d'utilisateur avec `CREATE USER` effectuée dans le TP2

✨ **Bonus** : Mettre en place un setup *master-master* où les deux serveurs sont répliqués en temps réel, mais les deux sont capables de traiter les requêtes.

**Installation :**

```
[alexis@dbslave ~]$ sudo dnf install mariadb-server -y
Last metadata expiration check: 0:03:10 ago on Fri 18 Nov 2022 02:59:19 PM CET.
Package mariadb-server-3:10.5.16-2.el9_0.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

**Démarrage du service mariadb :**
```
[alexis@dbslave ~]$ sudo systemctl start mariadb
[alexis@dbslave ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```
[alexis@dbslave ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

**Sur le Master (la permière DB) :**
```
[alexis@db ~]# sudo nano /etc/my.cnf.d/mariadb-server.cnf

[mysqld]
# add follows in [mysqld] section : get binary logs
log-bin=mysql-bin
# define server ID (uniq one)
server-id=101
```

```
[alexis@db ~]# sudo systemctl restart mariadb

[alexis@db ~]# mysql -u root -p
```
```sql
# create user (set any password for 'password' section)

MariaDB [(none)]> grant replication slave on *.* to repl_user@'%' identified by 'alexis'; 
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> flush privileges; 
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> exit
Bye
```

```
[alexis@dbslave ~]# sudo nano /etc/my.cnf.d/mariadb-server.cnf

[mysqld]
# add follows in [mysqld] section : get binary logs
log-bin=mysql-bin
# define server ID (uniq one)
server-id=102
# read only yes
read_only=1
# define own hostname
report-host=dbslave.tp3.linux
```

```
[alexis@dbslave ~]# systemctl restart mariadb

# create a directory and get Backup Data
[alexis@db ~]# mkdir /home/mariadb_backup
[alexis@db ~]# mariabackup --backup --target-dir /home/mariadb_backup -u root -p alexis
[00] 2021-08-03 16:14:27 Connecting to MySQL server host: localhost, user: root, password: set, port: not set, socket: not set
[00] 2021-08-03 16:14:27 Using server version 10.3.28-MariaDB
mariabackup based on MariaDB server 10.3.28-MariaDB Linux (x86_64)
[00] 2021-08-03 16:14:27 uses posix_fadvise().
[00] 2021-08-03 16:14:27 cd to /var/lib/mysql/
.....
.....
[00] 2021-08-03 16:14:30         ...done
[00] 2021-08-03 16:14:30 Redo log (from LSN 1640838 to 1640847) was copied.
[00] 2021-08-03 16:14:30 completed OK!
```

**stop MariaDB and remove existing data**

```
[alexis@dbslave ~]# systemctl stop mariadb

[alexis@dbslave ~]# rm -rf /var/lib/mysql/*
```

**transfered backup data**

```
[alexis@db ~]$ sudo zip -r mariadb_backup.zip /home/mariadb_backup/

[alexis@dbslave ~]$ sftp alexis@10.102.1.12
Connected to 10.102.1.12.
sftp> get mariadb_backup.zip
quit
[alexis@dbslave ~]$ sudo unzip mariadb_backup.zip
```

**run prepare task before restore task (OK if [completed OK])**

```
[alexis@dbslave ~]# mariabackup --prepare --target-dir mariadb_backup
mariabackup based on MariaDB server 10.3.28-MariaDB Linux (x86_64)
[00] 2021-08-03 16:23:30 cd to /home/alexis/mariadb_backup/
[00] 2021-08-03 16:23:30 open files limit requested 0, set to 1024
.....
.....
2021-08-03 16:23:30 0 [Note] InnoDB: Starting crash recovery from checkpoint LSN=1640838
[00] 2021-08-03 16:23:30 Last binlog file , position 0
[00] 2021-08-03 16:23:30 completed OK!
```

**run restore**
```
[alexis@dbslave ~]# mariabackup --copy-back --target-dir mariadb_backup
mariabackup based on MariaDB server 10.3.28-MariaDB Linux (x86_64)
[01] 2021-08-03 16:23:46 Copying ibdata1 to /var/lib/mysql/ibdata1
[01] 2021-08-03 16:23:46         ...done
.....
.....
[01] 2021-08-03 16:23:46 Copying ./xtrabackup_info to /var/lib/mysql/xtrabackup_info
[01] 2021-08-03 16:23:46         ...done
[00] 2021-08-03 16:23:46 completed OK!
```

```
[alexis@dbslave ~]# chown -R mysql. /var/lib/mysql

[alexis@dbslave ~]# systemctl start mariadb
```

**confirm [File] and [Position] value of master log**

```
[alexis@dbslave ~]# cat /root/mariadb_backup/xtrabackup_binlog_info
mysql-bin.000003        385     0-101-2
```

**set replication**

```
[alexis@dbslave ~]# mysql -u root -p
```

```sql
# master_host=(Master Host IP address)
# master_user=(replication user)
# master_password=(replication user password)
# master_log_file=([File] value confirmed above)
# master_log_pos=([Position] value confirmed above)

MariaDB [(none)]> change master to 
master_host='10.102.1.12',
master_user='repl_user',
master_password='alexis',
master_log_file='mysql-bin.000003',
master_log_pos=385;
Query OK, 0 rows affected (0.191 sec)
```

**start replication**

```sql
MariaDB [(none)]> start slave; 
Query OK, 0 rows affected (0.00 sec)
```

**show status**

```sql
MariaDB [(none)]> show slave status\G
*************************** 1. row ***************************
                Slave_IO_State: Waiting for master to send event
                   Master_Host: 10.102.1.12
                   Master_User: repl_user
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mysql-bin.000003
           Read_Master_Log_Pos: 385
                Relay_Log_File: mariadb-relay-bin.000002
                 Relay_Log_Pos: 555
         Relay_Master_Log_File: mysql-bin.000003
              Slave_IO_Running: Yes
             Slave_SQL_Running: Yes
               Replicate_Do_DB:
           Replicate_Ignore_DB:
            Replicate_Do_Table:
        Replicate_Ignore_Table:
       Replicate_Wild_Do_Table:
   Replicate_Wild_Ignore_Table:
                    Last_Errno: 0
                    Last_Error:
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 385
               Relay_Log_Space: 866
               Until_Condition: None
                Until_Log_File:
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File:
            Master_SSL_CA_Path:
               Master_SSL_Cert:
             Master_SSL_Cipher:
                Master_SSL_Key:
         Seconds_Behind_Master: 0
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error:
                Last_SQL_Errno: 0
                Last_SQL_Error:
   Replicate_Ignore_Server_Ids:
              Master_Server_Id: 101
                Master_SSL_Crl:
            Master_SSL_Crlpath:
                    Using_Gtid: No
                   Gtid_IO_Pos:
       Replicate_Do_Domain_Ids:
   Replicate_Ignore_Domain_Ids:
                 Parallel_Mode: optimistic
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
              Slave_DDL_Groups: 0
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 0
1 row in set (0.000 sec)
```
# TP3 : Amélioration de la solution NextCloud

Ce TP se présente sous forme modulaire : il est en plusieurs parties.  
Vous êtes libres d'attaquer les parties qui vous attirent le plus, et il n'est pas attendu de la part de tout le monde de poncer l'intégralité du contenu.

## Stack Web

# Module 1 : Reverse Proxy

Un reverse proxy est donc une machine que l'on place devant un autre service afin d'accueillir les clients et servir d'intermédiaire entre le client et le service.

# I. Intro

# II. Setup


**VM `proxy.tp3.linux`**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`
```
Installed:
  nginx-1:1.20.1-10.el9.x86_64     nginx-filesystem-1:1.20.1-10.el9.noarch     rocky-logos-httpd-90.11-1.el9.noarch

Complete!
```
- démarrer le service `nginx`
```
alexis@proxy ~]$ sudo systemctl start nginx
[alexis@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 16:09:35 CET; 4s ago
    Process: 32882 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 32883 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 32884 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 32885 (nginx)
      Tasks: 2 (limit: 5907)
     Memory: 1.9M
        CPU: 12ms
     CGroup: /system.slice/nginx.service
             ├─32885 "nginx: master process /usr/sbin/nginx"
             └─32886 "nginx: worker process"

Nov 17 16:09:35 proxy.tp3.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 17 16:09:35 proxy.tp3.linux nginx[32883]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 17 16:09:35 proxy.tp3.linux nginx[32883]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 17 16:09:35 proxy.tp3.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```
- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute
```
[alexis@proxy ~]$ sudo ss -laputrn | grep nginx
tcp   LISTEN 0      511                   0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=32886,fd=6),("nginx",pid=32885,fd=6))
tcp   LISTEN 0      511                      [::]:80           [::]:*    users:(("nginx",pid=32886,fd=7),("nginx",pid=32885,fd=7))
```
- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX
```
[alexis@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```
- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX
```
[alexis@proxy ~]$ ps -ef | grep nginx
root       32885       1  0 16:09 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      32886   32885  0 16:09 ?        00:00:00 nginx: worker process
```
- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine

➜ **Configurer NGINX**

- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`
```
    [alexis@proxy nginx]$ sudo cat conf.d/proxy.conf
server {
# On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.102.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`

➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `web.tp2.linux`
- c'est à dire que `web.tp2.linux` doit pointer vers l'IP de `proxy.tp3.linux`
```
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.102.1.13	web.tp2.linux
```
- autrement dit, pour votre PC :
  - `web.tp2.linux` pointe vers l'IP du reverse proxy
  - `proxy.tp3.linux` ne pointe vers rien
  - taper `http://web.tp2.linux` permet d'accéder au site (en passant de façon transparente par l'IP du proxy)

✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal. Une fois que c'est en place :


# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

- on génère une paire de clés sur le serveur `proxy.tp3.linux`
```
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
```
On change le nom des clés :
```
sudo mv server.crt web.tp2.linux.crt
sudo mv server.key web.tp2.linux.key
```
- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP
```
listen 443 ssl;
    ssl_certificate     /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;
```

On redirige les connexions en https directement :
```
server {

    listen 80 default_server;


    server_name _;


    return 301 https://$host$request_uri;
}
```

On change le fichier de config de nextcloud :

```
'overwrite.cli.url' => 'https://web.tp2.linux',
  'overwritehost' => 'web.tp2.linux',
  'overwriteprotocol' => 'https',
```

Et au final on arrive sur notre site en tapant : "https://web.tp2.linux"


## Module 7 : Fail2Ban

Fail2Ban c'est un peu le cas d'école de l'admin Linux, je vous laisse Google pour le mettre en place.

Faites en sorte que :

- si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
- vérifiez que ça fonctionne en vous faisant ban
- afficher la ligne dans le firewall qui met en place le ban
- lever le ban avec une commande liée à fail2ban

**Installation de Fail2Ban**

```
[alexis@db ~]$ sudo dnf install epel-release
Last metadata expiration check: 0:08:07 ago on Mon 21 Nov 2022 04:09:56 PM CET.
Dependencies resolved.
Installed:
  epel-release-9-4.el9.noarch

Complete!
```

```
[alexis@db ~]$ sudo dnf install fail2ban fail2ban-firewalld
Installed:
  esmtp-1.2-19.el9.x86_64                 fail2ban-1.0.1-2.el9.noarch           fail2ban-firewalld-1.0.1-2.el9.noarch
  fail2ban-sendmail-1.0.1-2.el9.noarch    fail2ban-server-1.0.1-2.el9.noarch    libesmtp-1.0.6-24.el9.x86_64
  liblockfile-1.14-9.el9.x86_64           python3-systemd-234-18.el9.x86_64

Complete!
```

**On lance le service Fail2Ban**
```
[alexis@db ~]$ sudo systemctl start fail2ban
[alexis@db ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-21 16:20:36 CET; 7s ago
       Docs: man:fail2ban(1)
    Process: 1492 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
   Main PID: 1493 (fail2ban-server)
      Tasks: 3 (limit: 5907)
     Memory: 12.3M
        CPU: 134ms
     CGroup: /system.slice/fail2ban.service
             └─1493 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Nov 21 16:20:36 db.tp2.linux systemd[1]: Starting Fail2Ban Service...
Nov 21 16:20:36 db.tp2.linux systemd[1]: Started Fail2Ban Service.
Nov 21 16:20:37 db.tp2.linux fail2ban-server[1493]: 2022-11-21 16:20:37,032 fail2ban.configreader   [1493]: WARNING 'al>
Nov 21 16:20:37 db.tp2.linux fail2ban-server[1493]: Server ready
```

```
[alexis@db ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
```

**On change la configuration de Fail2ban pour indiquer la durée du ban et le nombre de tentatives avant le ban :**
```
[alexis@db ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local


[alexis@db ~]$ sudo nano /etc/fail2ban/jail.local
[DEFAULT]
bantime = 1h
findtime = 1h
maxretry = 3


[alexis@db ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
```

alexisstart le service pour appliquer les changements :**
```
[alexis@db ~]$ sudo systemctl restart fail2ban
[alexis@db ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-21 16:31:06 CET; 3s ago
       Docs: man:fail2ban(1)
    Process: 1570 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
   Main PID: 1571 (fail2ban-server)
      Tasks: 3 (limit: 5907)
     Memory: 10.6M
        CPU: 70ms
     CGroup: /system.slice/fail2ban.service
             └─1571 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Nov 21 16:31:06 db.tp2.linux systemd[1]: Starting Fail2Ban Service...
Nov 21 16:31:06 db.tp2.linux systemd[1]: Started Fail2Ban Service.
Nov 21 16:31:06 db.tp2.linux fail2ban-server[1571]: 2022-11-21 16:31:06,316 fail2ban.configreader   [1571]: WARNING 'al>
Nov 21 16:31:06 db.tp2.linux fail2ban-server[1571]: Server ready
```

**Configuration pour Fail2Ban pour les connexions SSH :**
```
[alexis@db ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 1d
maxretry = 3
```

**Et on redémarre pour appliquer la conf**
```
alexis@db ~]$ sudo systemctl restart fail2ban
[alexis@db ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-21 16:33:16 CET; 5s ago
       Docs: man:fail2ban(1)
    Process: 1591 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
   Main PID: 1592 (fail2ban-server)
      Tasks: 5 (limit: 5907)
     Memory: 14.0M
        CPU: 144ms
     CGroup: /system.slice/fail2ban.service
             └─1592 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Nov 21 16:33:16 db.tp2.linux systemd[1]: Starting Fail2Ban Service...
Nov 21 16:33:16 db.tp2.linux systemd[1]: Started Fail2Ban Service.
Nov 21 16:33:16 db.tp2.linux fail2ban-server[1592]: 2022-11-21 16:33:16,668 fail2ban.configreader   [1592]: WARNING 'al>
Nov 21 16:33:16 db.tp2.linux fail2ban-server[1592]: Server ready
```


**On peut bien voir qu'on a une prison pour les connexions via SSHD :**
```
[alexis@db ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

**On vérifie bien qu'on a 3 chances avant de se faire ban :**
```
[alexis@db ~]$ sudo fail2ban-client get sshd maxretry
3
```

**On test maintenant pour voir si Fail2Ban marche en essayant de se connecter depuis la machine `web.tp2.linux` sur notre machine `db.tp3.linux` :**

```
[alexis@web ~]$ ssh alexis@10.102.1.12
alexis@10.102.1.12's password:
Permission denied, please try again.
alexis@10.102.1.12's password:
Permission denied, please try again.
alexis@10.102.1.12's password:
alexis@10.102.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[alexis@web ~]$ ssh alexis@10.102.1.12
ssh: connect to host 10.102.1.12 port 22: Connection refused
```

**On peut bien voir qu'après avoir raté 3 fois, on ne peut même plus se connecter en SSH la connexion est refusée et si l'on retourne voir dans la prison SSHD, on peut voir l'addresse IP de la machine `web.tp2.linux` dans la liste des ban :**
```
[alexis@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.102.1.11
```

Ensuite il me reste a me déban 

```
[alexis@db ~]$ sudo fail2ban-client unban 10.102.1.11
1
[alexis@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```



Ensuite on refait cette installation sur toutes nos autres machines 


