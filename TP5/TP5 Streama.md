# TP 5

## Databases
 * On se bascule sur le super utilisateur
```
[jeremy@mysql ~]$ sudo su
[sudo] password for jeremy:
```

* Créaton d'un répertoire pour enregistrer les fichiers : 
```
[root@mysql jeremy]# mkdir /data
[root@mysql jeremy]# mkdir /data/mysql
```

* On créer un utilisateur et le mettre dans le ```wheel```
```
[root@mysql jeremy]# sudo adduser mysql
[root@mysql jeremy]# passwd mysql
Changing password for user mysql.
New password:
[root@mysql jeremy]# sudo usermod -aG wheel mysql
```

* On installe ```mysql-server```
```
[root@mysql jeremy]# cd /data/mysql/
[root@mysql mysql]# sudo dnf install mysql-server

(...)

Complete!
```

* Configuration de MYSQL 
```
[root@mysql mysql]# sudo systemctl start mysqld
[root@mysql mysql]# sudo mysql_secure_installation

(...)

Press y|Y for Yes, any other key for No: y

(...)

Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 0

(...)

Estimated strength of the password: 50
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : y

(...)

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Success.

(...)

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y
Success.

(...)

Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y

(...)

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
Success.

All done!
```

* Creation de la database
```
[root@mysql mysql]# sudo mysql -p
Enter password:

(...)

mysql> create database streama;
Query OK, 1 row affected (0.01 sec)

mysql> exit
```

* On affiche la database
```
[root@mysql mysql]# sudo mysql -p

(...)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| streama            |
| sys                |
+--------------------+
5 rows in set (0.00 sec)

mysql> exit
```


```
[root@mysql mysql]# sudo mysql -p
Enter password:

(...)

mysql> CREATE USER 'user'@'%' IDENTIFIED BY 'jeremyjeremy';
Query OK, 0 rows affected (0.02 sec)

mysql> GRANT ALL PRIVILEGES ON streama.* TO 'user'@'%';
Query OK, 0 rows affected (0.01 sec)

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.01 sec)

mysql> exit
```

* On regarde le port sur lequel écoute 
```
[root@mysql mysql]# sudo ss -alptnr
State                Recv-Q               Send-Q                              Local Address:Port                                Peer Address:Port               Process
LISTEN               0                    128                                       0.0.0.0:22                                       0.0.0.0:*                   users:(("sshd",pid=697,fd=3))
LISTEN               0                    128                                          [::]:22                                          [::]:*                   users:(("sshd",pid=697,fd=4))
LISTEN               0                    70                                              *:33060                                          *:*                   users:(("mysqld",pid=3092,fd=21))
LISTEN               0                    151                                             *:3306                                           *:*                   users:(("mysqld",pid=3092,fd=24))
```

* On ouvre le port en question
```
[root@mysql mysql]# sudo firewall-cmd --add-port=3306/tcp --permanent
success
[root@mysql mysql]# sudo firewall-cmd --reload
success
```
* On affiche le port ouvert 
```
[root@mysql mysql]# sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[root@mysql mysql]#
```

## Streama

* On installe java 
```
[jeremy@streama ~]$ sudo dnf install java-1.8.0-openjdk java-1.8.0-openjdk-devel
```

* On vérifie 
```
[jeremy@streama ~]$ java -version
openjdk version "1.8.0_352"
OpenJDK Runtime Environment (build 1.8.0_352-b08)
OpenJDK 64-Bit Server VM (build 25.352-b08, mixed mode)
[jeremy@streama ~]$
```

 * On se bascule sur le super utilisateur
```
[jeremy@streama ~]$ sudo su
[sudo] password for jeremy:
```

* Créaton d'un répertoire pour enregistrer les fichiers : 
```
[root@streama jeremy]# mkdir /data
[root@streama jeremy]# mkdir /data/streama
[root@streama jeremy]# touch /data/streama/README.md
```

* On crée un utilisateur et le mettre dans le ```wheel``` et changer les permissions 
```
[root@streama streama]# sudo adduser streama
[root@streama streama]# passwd streama
Changing password for user mysql.
New password:
[root@streama streama]# sudo usermod -aG wheel streama
[root@streama streama]# sudo chown streama:streama /data/streama/ -R
```

* Installation de Streama
```
[root@streama streama]# cd /data/streama
[root@streama streama]# sudo su streama
[streama@streama streama]$ wget https://github.com/streamaserver/streama/releases/download/v1.10.4/streama-1.10.4.jar
[streama@streama streama]$ chmod ug+x streama-1.10.4.jar
[streama@streama streama]$ ln -s streama-1.10.4.jar streama.jar
```

* Installation de la ```application.yml```
```
[root@streama streama]# wget https://raw.githubusercontent.com/streamaserver/streama/master/docs/sample_application.yml
[root@streama streama]# mv sample_application.yml application.yml
[root@streama streama]# vi application.yml
```



*************************************

* On affiche la liste des firewalls
```
[root@streama streama]# sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* On ouvre le port en question
```
[root@streama streama]# sudo firewall-cmd --add-port=8080/tcp --permanent
success
[root@streama streama]# sudo firewall-cmd --reload
success
[root@streama streama]# sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* On démarre Streama
```
[root@streama streama]# sudo systemctl start streama

(...)

Grails application running at http://localhost:8080 in environment: production
```

```
http://10.104.1.2:8080
```
* Paramétrage de l'application Streama
![](https://i.imgur.com/SSAEaKA.png)

* Ajout de deux vidéos dans la bibliotheques

![](https://i.imgur.com/HmZMtdQ.png)

* La vidéo fonctionne nous pouvons maintenant visionner un film

![](https://i.imgur.com/6j93K0c.png)


Pensez a utiliser le fichier application.yml pour le fonctionnement de Streema.
projet fait avec jeremy
